<?php
	$projList = [];
	class Project{
		public $projId = "";
		public $projName = "";
		public $projDesc = "";
		public $visitDate = "";
		public $dueDate = "";
		public $wishCount = 0;
		public $wishGrant = 0;
		public $projUrl = "";
		public $projStatus = "";
	}
	
	$con=mysqli_connect("localhost","root","passw0rd1","avnettouch");
	
	$query = "SELECT * FROM T_PROJECTS";
	$result = mysqli_query($con,$query);
			
	while($row = mysqli_fetch_array($result)) {
		$proj = new Project();
		$proj->projId = $row['PROJ_ID'];
		$proj->projName = $row['PROJ_NAME'];
		$proj->projDesc = $row['PROJ_DESC'];
		$proj->visitDate = $row['PROJ_VISIT_DATE'];
		$proj->dueDate = $row['PROJ_DUE_DATE'];
		$proj->wishCount = $row['PROJ_WISH_COUNT'];
		$proj->wishGrant = $row['PROJ_WISH_GRANT'];
		$proj->projUrl = $row['PROJ_WL_URL'];
		$proj->projStatus = $row['PROJ_STATUS'];
		array_push($projList, json_encode($proj));
	}
	echo json_encode($projList);
	mysqli_close($con);
?>