var touchApp = angular.module('touchApp',['ngRoute']);

/*-- AJAX OBJECT --*/
var XMLHttpRequestObject = false;
if(window.XMLHttpRequest)
{
	XMLHttpRequestObject = new XMLHttpRequest();
}
else if(window.ActiveXObject){
	XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
}
/*--- END OF AJAX OBJECT --*/

touchApp.config(function ($routeProvider){
	$routeProvider
	.when('/',{
		controller: 'homeController',
		templateUrl: 'pages/home.html'
	})
	.when('/download',{
		controller: 'downloadController',
		templateUrl: 'pages/download.html'
	})
	.when('/upload',{
		controller: 'uploadController',
		templateUrl: 'pages/upload.html'
	})
	.when('/reminder',{
		controller: 'reminderController',
		templateUrl: 'pages/reminder.html'
	})
	.when('/settings',{
		controller: 'settingsController',
		templateUrl: 'pages/settings.html'
	})
	.otherwise({redirectTo:'/'});
});

touchApp.controller('homeController',function homeController($scope){
	
	$scope.projToDel = 0;
	
	$scope.loadProjects = function(){
		$scope.activeProjects = [];
		$scope.holdProjects = [];
		$scope.closedProjects = [];
	
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/homecontroller/getProjects.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;
					
					$scope.processResponse(JSON.parse(res));
				}		
			}
			XMLHttpRequestObject.send();
		}
	}
	$scope.processResponse = function(res){
		for(var i = 0; i < res.length; i++){
			var proj = JSON.parse(res[i]);
			var project = {};
			project.projName = proj.projName;
			project.visitDate = proj.visitDate;
			project.projId = proj.projId;
			
			if(proj.projStatus == "active"){
				$scope.activeProjects.push(project);
			}
			else if(proj.projStatus == "hold"){
				$scope.holdProjects.push(project);
			}
			else{
				$scope.closedProjects.push(project);
			}
			$scope.$apply();
		}
	}
	
	$scope.closeProject = function(projId){
		$scope.changeProjStatus(projId, "closed");
	}
	$scope.activateProject = function(projId){
		$scope.changeProjStatus(projId, "active");
	}
	$scope.deleteProject = function(projId){
		$scope.projToDel = projId;
		$scope.showPopUp();
	};
	$scope.showPopUp = function(){
		document.getElementById("popUp").style.display = "block";
	};
	$scope.popUpClicked = function(option){
		console.log(option);
		if(option=="yes"){
			$scope.changeProjStatus($scope.projToDel, "delete");
		}
		else{
			document.getElementById("popUp").style.display = "none";
			$scope.projToDel = undefined;
		}
	};
	$scope.changeProjStatus = function(projId, status){
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/homecontroller/changeProjectStatus.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;
					$scope.message = res;
					$scope.displayMsg = "show";
					document.getElementById("popUp").style.display = "none";
					$scope.$apply();
					$scope.loadProjects();
				}		
			}
			XMLHttpRequestObject.send("projId=" +projId+ "&status=" +status);
		}
	}
	$scope.loadProjects();
});
/* --- DOWNLOADS CONTROLLER -- */
touchApp.controller('downloadController',function downloadController($scope){

	
	$scope.loadProjects = function(){
		$scope.projects = [];
	
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/downloadcontroller/downloadWishlist.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;
					$scope.processResponse(JSON.parse(res));
				}		
			}
			XMLHttpRequestObject.send();
		}
	};
	$scope.processResponse = function(res){
		for(var i = 0; i < res.length; i++){
			var proj = JSON.parse(res[i]);
			var project = {};
			project.projName = proj.projName;
			project.uploadDate = proj.uploadDate;
			project.url = proj.url;
			project.projOwner = proj.projOwner;
			project.status = proj.status;
			project.projId = proj.projId;
			
			$scope.projects.push(project);
			$scope.$apply();
		}
	};
	
	
	$scope.changeStatus = function(projId, status){
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/downloadcontroller/changeProjectStatus.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;

					$scope.loadProjects();
				}		
			}
			XMLHttpRequestObject.send("projId=" +projId+ "&status=" +status);
		}
	};
	
	
	$scope.loadProjects();
});

/* --- UPLOADS CONTROLLER -- */
touchApp.controller('uploadController',function uploadController($scope){
	
});

/* --- REMINDER CONTROLLER -- */
touchApp.controller('reminderController',function reminderController($scope){
	$scope.loadDueList = function(){
		$scope.remList = [];
	
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/remindercontroller/dueList.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;
					var list = JSON.parse(res);
					for(var i = 0; i < list.length; i++){
						var proj = JSON.parse(list[i]);
						var item = {};
						item.name = proj.wishItemName;
						item.qty = proj.wishItemQty;
						item.oth = proj.wishItemOthers;
						item.mail = proj.contributorMail;
						item.uname = proj.contributorName;
						
						$scope.remList.push(item);
						$scope.$apply();
					}
				}		
			}
			XMLHttpRequestObject.send();
		}
	};
	
	$scope.mailTo = function(mailId, all){
		if(all=="true"){
			all == "true";
		}
		else{
			all == "false";
		}
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/remindercontroller/reminder.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res = XMLHttpRequestObject.responseText;
					console.log(res);
					$scope.$apply();
				}		
			}
			XMLHttpRequestObject.send("mailId=" +mailId+"&all="+all);
		}
	}
	
	$scope.loadDueList();
});

/* --- SETTINGS CONTROLLER --- */
touchApp.controller('settingsController',function settingsController($scope){
	$scope.curPass = "";
	$scope.newPass = "";
	$scope.retypePass = "";
	
	$scope.changePassword = function(){
		if(XMLHttpRequestObject){
			XMLHttpRequestObject.open("POST",'server/settingscontroller/changePassword.php');
			XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			XMLHttpRequestObject.onreadystatechange=function()
			{
				if(XMLHttpRequestObject.readyState==4 && XMLHttpRequestObject.status==200)
				{
					var res=XMLHttpRequestObject.responseText;
					$scope.message = res;
					$scope.displayMsg = "show"
					$scope.$apply();
				}		
			}
			XMLHttpRequestObject.send("cur_pwd=" +$scope.curPass+ "&new_pwd=" +$scope.newPass+" &re_pwd=" +$scope.retypePass);
		}
	}
});
function maskName(){
	document.getElementById("selectedFile").value = document.getElementById("myFile").value;
}